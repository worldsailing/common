<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\BundleResultSet;

/**
 * Class CompositeEntityResultSet
 * @package worldsailing\Common
 */
class CompositeEntityResultSet implements CompositeResultSetInterface
{
    /**
     * @var mixed|null
     */
    protected $data;

    /**
     * @var bool
     */
    protected $fromCache;

    /**
     * @var mixed
     */
    protected $lastModified;

    /**
     * CompositeEntityResultSet constructor.
     * @param mixed|null $data
     * @param bool $fromCache
     * @param mixed|null $lastModified
     */
    function __construct($data = null, $fromCache = false, $lastModified = null)
    {
        $this->data = $data;
        $this->fromCache = $fromCache;
        $this->lastModified = $lastModified;
    }

    /**
     * @return mixed|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isData()
    {
        return ($this->data === null || $this->data === false) ? false : true;
    }

    /**
     * @return bool
     */
    public function isFromCache()
    {
        return $this->fromCache;
    }

    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }


}



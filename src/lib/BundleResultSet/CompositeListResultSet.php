<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\BundleResultSet;

/**
 * Class CompositeListResultSet
 * @package worldsailing\Common
 */
class CompositeListResultSet implements CompositeResultSetInterface
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var int
     */
    protected $countAll;

    /**
     * @var int
     */
    protected $affected;

    /**
     * @var bool
     */
    protected $fromCache;

    /**
     * CompositeListResultSet constructor.
     * @param array $data
     * @param bool $fromCache
     */
    function __construct($data = [], $fromCache = false)
    {
        $this->data = $data;
        $this->affected = count($data);
        $this->fromCache = $fromCache;
    }

    /**
     * @return int
     */
    public function getCountAll()
    {
        return $this->countAll;
    }

    /**
     * @param int $countAll
     * @return CompositeListResultSet
     */
    public function setCountAll($countAll)
    {
        $this->countAll = $countAll;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isData()
    {
        return ($this->data === null || $this->data === false || $this->affected == 0) ? false : true;
    }

    /**
     * @return int
     */
    public function getAffected()
    {
        return $this->affected;
    }

    /**
     * @return bool
     */
    public function isFromCache()
    {
        return $this->fromCache;
    }

}


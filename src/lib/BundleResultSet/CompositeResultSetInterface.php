<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\BundleResultSet;

interface CompositeResultSetInterface
{

    /**
     * @return array|mixed
     */
    public function getData();

    /**
     * @return bool
     */
    public function isData();

}


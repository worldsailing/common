<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\ApiResultSet;

use worldsailing\Common\ApiResultSet\core\AbstractEntityResultSet;

/**
 * Class EntityResultSet
 * @package worldsailing\Common\ApiResultSet
 */
class EntityResultSet extends AbstractEntityResultSet
{
    /**
     * @param $resource
     * @return void
     */
    public function describe($resource)
    {
        $this->vars = [

        ];
    }
}
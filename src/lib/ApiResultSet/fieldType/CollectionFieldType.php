<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\ApiResultSet\fieldType;

use worldsailing\Common\ApiResultSet\core\AbstractCollectionField;
use worldsailing\Common\ApiResultSet\core\CollectionFieldInterface;

/**
 * Class CollectionFieldType
 * @package worldsailing\Common\ApiResultSet\fieldType
 */
class CollectionFieldType extends AbstractCollectionField implements CollectionFieldInterface
{
    /**
     * @return array
     */
    public function map()
    {
        $result = [];
        if ($this->items && is_array($this->items)) {
            foreach ($this->items as $item) {
                $result[] = $item->map();
            }
        }
        return $result;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }
}

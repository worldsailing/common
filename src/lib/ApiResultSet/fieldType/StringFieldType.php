<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\ApiResultSet\fieldType;

use worldsailing\Common\ApiResultSet\core\AbstractFieldType;
use worldsailing\Common\ApiResultSet\core\FieldTypeInterface;

/**
 * Class StringFieldType
 * @package worldsailing\Common\ApiResultSet\fieldType
 */
class StringFieldType extends AbstractFieldType implements FieldTypeInterface
{

    /**
     * @return null|string
     */
    public function value()
    {
        if ($this->value === null) {
            return null;
        } else {
            return (string) $this->value;
        }
    }

}

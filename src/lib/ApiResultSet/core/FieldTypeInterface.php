<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\ApiResultSet\core;
/**
 * Interface FieldTypeInterface
 * @package worldsailing\Common\ApiResultSet\core
 */
interface FieldTypeInterface
{

    /**
     * @return null|mixed
     */
    public function value();

    /**
     * @return string
     */
    public function name();

}


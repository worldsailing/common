<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\ApiResultSet\core;

/**
 * Class AbstractCollectionField
 * @package worldsailing\Common\ApiResultSet\core
 */
abstract class AbstractCollectionField
{

    /**
     * @var
     */
    protected $name;

    /**
     * @var ResultSetInterface[]
     */
    protected $items;

    /**
     * AbstractCollectionField constructor.
     * @param $name
     * @param $closure
     * @param array $resource
     */
    public function __construct($name, $closure, $resource = [] )
    {
        $this->name = $name;
        foreach ($resource as $item) {
            $this->items[] = $closure($item);
        }
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return ResultSetInterface[]
     */
    public function getItems()
    {
        return $this->items;
    }

}


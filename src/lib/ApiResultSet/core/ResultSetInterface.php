<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\ApiResultSet\core;
/**
 * Interface ResultSetInterface
 * @package worldsailing\Common\ApiResultSet\core
 */
interface ResultSetInterface
{

    /**
     * @return void
     */
    public function describe($resource);

    /**
     * @return string
     */
    public function name();

    /**
     * @return array
     */
    public function map();

    /**
     * @return string
     */
    public function toJson();
}


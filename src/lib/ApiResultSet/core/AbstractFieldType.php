<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\ApiResultSet\core;

/**
 * Class AbstractFieldType
 * @package worldsailing\Common\ApiResultSet\core
 */
abstract class AbstractFieldType
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var null
     */
    protected $value;

    /**
     * AbstractFieldType constructor.
     * @param $name
     * @param null|mixed $value
     */
    public function __construct($name, $value = null)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

}


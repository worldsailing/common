<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\ApiResultSet\core;

/**
 * Class AbstractListResultSet
 * @package worldsailing\Common\ApiResultSet\core
 */
abstract class AbstractListResultSet extends AbstractResultSet
{

    /**
     * AbstractListResultSet constructor.
     * @param $name
     * @param null $resource
     * @throws \Exception
     */
    public function __construct($name, $resource = null)
    {
        parent::__construct($name, $resource);
        if (! ($this->vars instanceof CollectionFieldInterface)) {
            $reflect = new \ReflectionClass($this->vars);
            throw new \Exception('AbstractListResultSet only can handle AbstractCollectionField instance. [' . $reflect->getShortName() . '] given.' );
        }
    }

    /**
     * @return array
     */
    public function map()
    {
        $vars = [];
        foreach ($this->vars->getItems() as $var) {
            if ($var instanceof FieldTypeInterface) {
                $vars[] = $var->value();
            } elseif ( $var instanceof CollectionFieldInterface) {
                $vars[] = $var->map();
            } elseif ( $var instanceof ResultSetInterface) {
                $vars[] = $var->map();
            }
        }
        return $vars;
    }
}


<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\ApiResultSet\core;

/**
 * Class AbstractResultSet
 * @package worldsailing\Common\ApiResultSet\core
 */
abstract class AbstractResultSet implements ResultSetInterface
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var
     */
    protected $vars;

    /**
     * AbstractResultSet constructor.
     * @param $name
     * @param null $resource
     */
    public function __construct($name, $resource = null)
    {
        $this->name = $name;
        $this->describe($resource);
    }

    /**
     * @param $resource
     * @return void
     */
    abstract public function describe($resource);

    /**
     * @return array
     */
    abstract public function map();

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function toJson()
    {
        $vars = $this->map();

        return json_encode($vars);
    }
}


<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\ApiResultSet\core;
/**
 * Interface CollectionFieldInterface
 * @package worldsailing\Common\ApiResultSet\core
 */
interface CollectionFieldInterface
{

    /**
     * @return array
     */
    public function map();

    /**
     * @return integer
     */
    public function count();

    /**
     * @return string
     */
    public function name();

    /**
     * @return array
     */
    public function getItems();
}


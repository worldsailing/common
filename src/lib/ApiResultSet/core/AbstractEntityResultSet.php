<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Common\ApiResultSet\core;

/**
 * Class AbstractEntityResultSet
 * @package worldsailing\Common\ApiResultSet\core
 */
abstract class AbstractEntityResultSet extends AbstractResultSet
{

    /**
     * @return array
     */
    public function map()
    {
        $vars = [];

        foreach ($this->vars as $key => $var) {
            if ($var instanceof FieldTypeInterface) {
                $vars[$var->name()] = $var->value();
            } elseif ( $var instanceof CollectionFieldInterface) {
                $vars[$var->name()] = $var->map();
            } elseif ( $var instanceof ResultSetInterface) {
                $vars[$var->name()] = $var->map();
            }
        }
        return $vars;
    }
}


<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Common\Exception;

use \Exception;
use worldsailing\Common\ApiResultSet\Core\ResultSetInterface;

class WsServiceException extends Exception
{

    /**
     * @var ResultSetInterface
     */
    protected $data;

    /**
     * @param string $message
     */
    public function __construct($message, $code = 400)
    {
        parent::__construct($message, $code);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": {$this->message}\n";
    }

    /**
     * @param ResultSetInterface|null $data
     * @return $this
     */
    public function __setData(ResultSetInterface $data = null)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function __map()
    {
        if ($this->data && $this->data instanceof ResultSetInterface) {
            return $this->data->map();
        } else {
            return [];
        }
    }

    /**
     * @return string
     */
    public function __toJson()
    {
        if ($this->data && $this->data instanceof ResultSetInterface) {
            return $this->data->toJson();
        } else {
            return '';
        }
    }
}

# worldsailing Common 

### PHP library description
Bundle for "Shared objects implementations" to World Sailing microservices based on Silex.

  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://worldsailing@bitbucket.org/worldsailing/common.git"
    }
  ],
  "require": {
    "worldsailing/common": "dev-master"
  }
}
```
 
### License

All right reserved
